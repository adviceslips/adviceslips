#!/home/adviceslips/slips_env/bin/python3

import os 
import requests # Send http requests
from dotenv import load_dotenv # Load env vars
from twilio.rest import Client # Send sms through twilio
from pymongo import MongoClient # Connect to database

# Load env variables
load_dotenv('.env')

# Get contacts from database
print('Getting contacts from database')

# Connect to database, get collection
client = MongoClient(os.environ.get('MONGO_CONNECTION'))
db = client[os.environ.get('MONGO_DB')]
collection = db.contacts

# Get contacts
contacts = collection.find({})

print('Closing connection to database')

# Close db connection 
client.close()

# Load twilio account sid & auth
twilio_sid = os.environ.get('ACCOUNT_SID')
twilio_auth = os.environ.get('AUTH_TOKEN')
twilio_number = '+12057408819'

print('Connecting to the Twilio client')

# Twilio client
client = Client(twilio_sid, twilio_auth)

print('Starting to send sms messages to contacts')

# Loop through everyone in contacts
for person in contacts:
    # Get a slip
    response = requests.get('https://api.adviceslip.com/advice')
    result = response.json()
    slip = result['slip']

    # Contact details
    name = person['name']
    number = person['number']
    isAuthed = person['isAuthed']
    advice_message = f"Daily advice:\n{slip['advice']}"

    print(f'Sending text to {name}')

    # User has 2FA their phone number
    if isAuthed:

        # Send text
        result = client.messages.create(
            body= advice_message,
            from_= twilio_number,
            to = number
        )

        print(f'Sent text to {name}')
    else:
        print(f'Did not send text to {name}, not authenticated')

print('Finshed sending sms messages to contacts')