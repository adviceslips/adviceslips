# AdviceSlips random SMS

This is a simple script that send grabs a piece of advice from the [Advice slip API](https://api.adviceslip.com/) and sends a SMS to our users using Twilio and Mongo.

  
## Getting Started

*Please make sure that you have the python environment installed.*

1. Clone this repository.
2. Navigate to the root of this directory and create a virtual env: `python3 -m venv env_adviceslips`
3. Use the virtual environment: `source env_adviceslips/bin/activate`
4. Install dependencies: `pip3 install -r requirements.txt`

  
### Environment variables

- `MONGO_CONNECTION`
- `MONGO_DB`
- `ACCOUNT_SID`
- `AUTH_TOKEN`